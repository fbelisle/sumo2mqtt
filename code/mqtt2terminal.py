#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" 
Publication des informations de la simulation SUMO 
"""
from __future__ import print_function
import paho.mqtt.client as paho
import json 

def on_message(mosq, obj, msg):
    m_in = json.loads(msg.payload.decode())
    print (m_in)

def on_publish(mosq, obj, mid):
    pass

if __name__ == '__main__':
    client = paho.Client()
    client.on_message = on_message
    client.on_publish = on_publish

    client.connect("127.0.0.1", 1883, 60)
    client.subscribe("sumo/simulation", 0)

    # CHANGER ICI POUR AUTRE SERVEUR, par exemple...
    #client.connect("broker.hivemq.com", 1883, 60)
    #client.subscribe("sumo/simulation", 0)
    
    while client.loop() == 0:
        pass



