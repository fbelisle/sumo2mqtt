# -*- coding: utf-8 -*-

# usage : python traci2mqtt.py ../sumo/port/port.sumocfg localhost sumo/simulation 3600 
# to see the data sent to the channel, run :

# $ code/mqtt2terminal.py

from __future__ import print_function
import os, sys
import argparse

import paho.mqtt.client as paho
import paho.mqtt.publish as publish
import json

try:
    import traci
except ModuleNotFoundError:
    if 'SUMO_HOME' in os.environ :
        tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
        sys.path.append(tools)
    else:
        sys.exit("Déclarer la variable d'environnement : 'SUMO_HOME' et vérifier l'installation de SUMO")

import traci

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='sumo simulation results broadcast through MQTT')
    parser.add_argument('model', metavar='MODEL', type=str,
                        help='Model file (*.sumocfg) to simulate')
    parser.add_argument('host', metavar='HOST', type=str, default='localhost',
                        help='Where the simulation results are sent (broker)')
    parser.add_argument('topic', metavar='TOPIC', type=str, default='sumo/simulation',
                        help='On which topic the simulation results are sent')
    parser.add_argument('--gui', dest='sumoBinary',nargs='?',
                        const='sumo-gui', default='sumo',
                        help='Run sumo in GUI mode')
    parser.add_argument('endtime', metavar='ENDTIME',
                        type=int,
                        help='Simulation end time')
    
    args = parser.parse_args()
    sumoBinary = os.path.join(os.environ['SUMO_HOME'], "bin", args.sumoBinary)
    sumoCmd = [sumoBinary, "-c", args.model] 

    traci.start(sumoCmd)

    step = 0
    while step < args.endtime: 
        traci.simulationStep()
        step += 1
        for detector in traci.inductionloop.getIDList():
            dv = traci.inductionloop.getLastStepVehicleNumber(detector) 
            if dv > 0 :
                message = json.dumps(                
                {
                    "Format" : "ODNF1",
                    "Desc" : "Comptage en simulation",
                    "CreateUTC" : step,
                    "ExpireUTC" : -1,
                    "Unit" : "vehicule",
                    "Status" : "Good",
                    "Value" :
                    {
                        "Detector" : detector,
                        "Comptage" : dv
                    }
                })

                publish.single(topic=args.topic, payload=message, hostname=args.host)
    traci.close()
